# CountdownTimer
This is part of the tutorial series for front-end web development.

## Learning outcomes
1. Angular cli
2. Generating components
3. Utilizing components to compose a UI
4. Styling component
5. Implementing basic logic within components

## Enhancements
1. Allow the user to update the label
2. Allow the user to set the end date
3. Randomize the background image base on label
4. Persist the user preferences in a storage (Cookies/Session storage/Local storage)

## Run the example
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Background Image
Free to use image assets from [unsplash.com](https://unsplash.com/)
