import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss']
})
export class TimerComponent implements OnInit {

  public days: number;
  public hours: number;
  public minutes: number;
  public seconds: number;

  @Input() public endDate: Date = new Date(2020, 11, 25);

  constructor() {
  }

  public ngOnInit(): void {
    // This could be a service
    setInterval(() => {
      const now: number = new Date().getTime();
      const timeDiff: number = this.endDate.getTime() - now;
      // get the days from the time
      this.days = Math.floor(timeDiff / (1000 * 60 * 60 * 24));
      // get the hours from the time
      this.hours = Math.floor(
        (timeDiff % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
      );
      // get the minutes from the time
      this.minutes = Math.floor((timeDiff % (1000 * 60 * 60)) / (1000 * 60));
      // get the seconds from the time
      this.seconds = Math.floor((timeDiff % (1000 * 60)) / 1000);
    }, 1000);
  }

}
